import axios from 'axios'
import {API_URL} from '../../Constants'

class DaitumService {


    getResult(input, subsetsize){
        return axios.get(`${API_URL}/daitum/input/${input}/subsetsize/${subsetsize}`);
    }

}

export default new DaitumService()