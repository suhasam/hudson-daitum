import logo from './logo.svg';
import './App.css';
import Daitum from './components/daitum/Daitum';


function App() {
  return (
    <div className="App">
      <Daitum/>
    </div>
  );
}

export default App;
