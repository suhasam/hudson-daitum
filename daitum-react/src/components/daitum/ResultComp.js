
import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import { Button, Container, Form, FormGroup, Input, Label } from 'reactstrap';
import AppNavbar from './AppNavbar';


class ResultComp extends Component {
    constructor(props) {
        super(props)

        this.state = {    
            subset: '',
            sum:''   
        }

       
    }


    async componentDidMount() {
        this.setState({
            subset: this.props.match.params.subset,
            sum:this.props.match.params.sum  
        })  
    }




    render() {

     //   let { subset, sum} = this.state
        //let targetDate = this.state.targetDate

        return (
            <div>
                <h1>Result</h1>
                <div>
                     
                     The subset : <strong>{this.state.subset}</strong>
                     <br/>
                     The sum XXX : <strong>{this.state.sum}</strong>

                </div>
            </div>
        )
    }
}

export default withRouter(ResultComp);
