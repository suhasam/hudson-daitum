import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import { Button, Container, Form, FormGroup, Input, Label } from 'reactstrap';
import AppNavbar from './AppNavbar';

class InputComp extends Component {

    emptyItem = {
        input: '',
        subsetsize: '',
        subset: '',
        sum: ''
    };

    constructor(props) {
        super(props);
        this.state = {
            item: this.emptyItem
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    async componentDidMount() {
      //  if (this.props.match.params.id !== 'new') {
            const result = await (await fetch(`/daitum/input/${this.props.match.params.input}/subsetsize/${this.props.match.params.subsetsize}`)).json();
            this.setState({item: result});
       // }
    }

    handleChange(event) {
        const target = event.target;
        const value = target.value;
        const name = target.name;
        let item = {...this.state.item};
        item[name] = value;
        this.setState({item});
    }

async handleSubmit(event) {
    event.preventDefault();
    const {item} = this.state;

    console.log("Submitting ---"+this.state.item.input);

    await fetch(`/daitum/input/${this.state.item.input}/subsetsize/${this.state.item.subsetsize}`).then(response => response.json())
            .then(data => this.setState({item: data}));


    this.props.history.push(`/result/${this.state.item.subset}/${this.state.item.sum}`)
}

    render() {
        const {item} = this.state;
        const title = <h2>Daitum Test</h2>;

        return <div>
            <AppNavbar/>
            <Container>
                {title}
                <Form onSubmit={this.handleSubmit}>
                    <FormGroup>
                        <Label for="name">Input --- </Label>
                        <Input type="text" name="input" id="input" value={item.input || ''}
                               onChange={this.handleChange} autoComplete="input"/>
                    </FormGroup>
                    <FormGroup>
                        <Label for="email">Subset Size --- </Label>
                        <Input type="text" name="subsetsize" id="subsetsize" value={item.subsetsize || ''}
                               onChange={this.handleChange} autoComplete="subsetsize"/>
                    </FormGroup>
                    <FormGroup>
                        <Button color="primary" type="submit">Generate Result</Button>{' '}
                       
                    </FormGroup>
                </Form>
            </Container>
        </div>
    }
}

export default withRouter(InputComp);