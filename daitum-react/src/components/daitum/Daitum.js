import React, {Component} from 'react';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import InputComp from './InputComp';
import ResultComp from './ResultComp';




class Daitum extends Component {
    render() {
      return (
          <Router>
            <Switch>
            <Route path='/' exact={true} component={InputComp}/> 
            <Route path="/result/:subset/:sum" component={ResultComp}/>            
            </Switch>
          </Router>
      )
    }
  }
  
  export default Daitum;