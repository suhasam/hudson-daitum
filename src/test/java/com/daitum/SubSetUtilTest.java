package com.daitum;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;

import com.daitum.util.OutputDataUtil;

import com.daitum.model.dto.DaitumDto;

class SubSetUtilTest {

	@Test
	void testSubSetUtil() {
		
		List<Integer> inputdata = Arrays.asList(2, 2, -3, 0, 2);
		
		OutputDataUtil outPutDatUtil = new OutputDataUtil();
		
		DaitumDto output = outPutDatUtil.generateOutPutData(inputdata, 2);
		
		System.out.println(output);
	}

}
