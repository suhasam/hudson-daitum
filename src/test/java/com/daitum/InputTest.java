package com.daitum;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;

class InputTest {

	@Test
	void test() {
		
		
		String ranks = "2,-3,4,5,6,7";
		
		List<Integer> convertedRankList = Stream.of(ranks.split(","))
				  .map(String::trim)
				  .map(Integer::parseInt)
				  .collect(Collectors.toList());
		
		System.out.println(convertedRankList);
	}

}
