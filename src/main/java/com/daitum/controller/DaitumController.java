package com.daitum.controller;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.daitum.model.dto.DaitumDto;
import com.daitum.services.DaitumService;


@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/daitum")
public class DaitumController {

	private final DaitumService daitumService;

	@Autowired
	public DaitumController(DaitumService daitumService) {
		this.daitumService = daitumService;
	}

	@GetMapping("/input/{input}/subsetsize/{subsetsize}")
	public DaitumDto getResult(@PathVariable String input, @PathVariable String subsetsize) {

		if (input == null || input.trim().isEmpty()) {
			throw new IllegalArgumentException("Invalid input");
		}

		if (subsetsize == null || subsetsize.trim().isEmpty()) {
			throw new IllegalArgumentException("Invalid subsetsize");
		}

		List<Integer> inputData = Stream.of(input.trim().split(",")).map(String::trim).map(Integer::parseInt)
				.collect(Collectors.toList());

		Integer sublistsize = Integer.valueOf(subsetsize);

		DaitumDto outputDto = daitumService.getSubSetOutData(inputData, sublistsize);

		outputDto.setInput(input);
		outputDto.setSubsetsize(subsetsize);

		return outputDto;
	}

}
