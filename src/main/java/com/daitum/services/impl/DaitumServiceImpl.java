package com.daitum.services.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.daitum.model.dto.DaitumDto;
import com.daitum.services.DaitumService;
import com.daitum.util.OutputDataUtil;

@Service
public class DaitumServiceImpl implements DaitumService{

	@Override
	public DaitumDto getSubSetOutData(List<Integer> inputdata, int subSetSize) {
		
		 
		OutputDataUtil outPutDatUtil = new OutputDataUtil();
		DaitumDto daitumDto = outPutDatUtil.generateOutPutData(inputdata, subSetSize);
		return daitumDto;
	}

}
