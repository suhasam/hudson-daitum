package com.daitum.services;

import java.util.List;

import com.daitum.model.dto.DaitumDto;

public interface DaitumService {
	
	
	public DaitumDto getSubSetOutData(List<Integer> inputdata, int subSetSize);

}
