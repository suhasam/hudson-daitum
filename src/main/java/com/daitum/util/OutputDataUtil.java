package com.daitum.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import com.daitum.model.dto.DaitumDto;

public class OutputDataUtil {
	
	
    
	
	 public DaitumDto generateOutPutData(List<Integer> inputdata, int subSetSize )
	 {
		 SubSetUtil  subSetUtil = new SubSetUtil();
		 SortAndFindUtil sortAndFindUtil = new SortAndFindUtil();	
		 DaitumDto outPutDto = new DaitumDto();
		 
		 HashMap<Integer, ArrayList<Integer>> hMapSubSetWithSum = subSetUtil.generate(inputdata, subSetSize);		 
		 List<Integer> sumOfSubSetList = new ArrayList<Integer>(hMapSubSetWithSum.keySet());		 
		 Integer closeToZeroSum = sortAndFindUtil.findMinSum(sumOfSubSetList, sumOfSubSetList.size());
		 
		 ArrayList<Integer> outPutSubSet = hMapSubSetWithSum.get(closeToZeroSum);
		 
		 
		 String resultSubSet = outPutSubSet.stream()
			      .map(n -> String.valueOf(n))
			      .collect(Collectors.joining("-", "{", "}"));
		 
		 
		 String strCloseToZeroSum = closeToZeroSum.toString();
		 
		 
		// outPutDto.setOutPutSubSet(outPutSubSet);
		// outPutDto.setSubSetSum(closeToZeroSum);
		 
		 outPutDto.setSubset(resultSubSet);
		 outPutDto.setSum(strCloseToZeroSum);
		 
		 return outPutDto;
				 
	 
	 }
	
	
	/*
	 * SetRecursiveCombinationGenerator generator = new
	 * SetRecursiveCombinationGenerator(); HashMap<Integer, ArrayList<Integer>> hMap
	 * = generator.generate(N, R);
	 * 
	 * 
	 * System.out.println("SIZE --- > "+sum1.size());
	 * 
	 * 
	 * hMap.forEach((k, v) -> System.out.println("Key : " + k + ", Value : " + v));
	 * 
	 * List<Integer> intSum = new ArrayList<Integer>(hMap.keySet());
	 * 
	 * Integer ss = findMinSum(intSum, intSum.size());
	 * 
	 * 
	 * System.out.println(" GHGH -- > "+hMap.get(ss));
	 */

}
