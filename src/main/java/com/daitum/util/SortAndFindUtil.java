package com.daitum.util;

import java.util.Collections;
import java.util.List;

public class SortAndFindUtil {
	
	
	 public Integer findMinSum(List<Integer> intSum, int n)
	    {
		 
		 

	        
	    	Collections.sort(intSum);
	    	
	        int min = Integer.MAX_VALUE;
	        int x = 0, y = 0;
	         
	        for(int i = 1; i < n; i++)
	        {
	             
	            // Absolute value shows how close
	            // it is to zero
	            if (Math.abs(intSum.get(i - 1)) <= min)
	            {
	                 
	                // If found an even close value
	                // update min and store the index
	                min = Math.abs(intSum.get(i - 1));
	                x = i - 1;
	                y = i;
	            }
	        }
	        System.out.println("The two elements whose " +
	                           "sum is minimum is " +
	                           intSum.get(x) );
	        
	        return intSum.get(x);
	    }

}
