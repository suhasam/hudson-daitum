package com.daitum.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class SubSetUtil {
	
	
	
	
    public HashMap<Integer, ArrayList<Integer>> generate(List<Integer> inputdata, int subSetSize ) {
    	
		/*
		 * HashMap<Integer, ArrayList<Integer>> hMap = new HashMap<Integer,
		 * ArrayList<Integer>>();
		 * 
		 * int arr[] = { 10, 20, 30, 40, 50 };
		 */
         
       // List<Integer> inputdata = Arrays.asList(2, 2, -3, 0, 2);
       
         //int data[] = new int[r];
         
        //  n = arr.length;
       
    
        List<ArrayList<Integer>> combinations = new ArrayList<ArrayList<Integer>>();
        HashMap<Integer, ArrayList<Integer>> hMap = new HashMap<Integer,
       		  ArrayList<Integer>>();
        
        ArrayList<Integer> subList = new ArrayList<Integer>();        
        int lastIndexOfInputArray = inputdata.size() - 1;
        helper(hMap,combinations, subList, 0, lastIndexOfInputArray, 0, inputdata, subSetSize);
        return hMap;
    }
	
	
	
	  private void helper(HashMap<Integer, ArrayList<Integer>> hMap, List<ArrayList<Integer>> combinations, ArrayList<Integer> subList, int start, int end, int index, List<Integer> integers, int subSetSize) {
	       
	    	
	    	
	    	if (index == subSetSize) {
	           
	           // System.out.println("index p -"+index);
	           // System.out.println("before adding to parent -"+subList);
	            
	            
	           
	          //  combinations.add(subList);
	            ArrayList<Integer> newEntry = new ArrayList<Integer>();
	            
	             newEntry =      (ArrayList<Integer>) subList.stream()
	            .collect(Collectors.toList());
	            
	            
	            Integer sum = newEntry.stream().collect(Collectors.summingInt(Integer::intValue));
	            
	       //     System.out.println(" SUM --> "+sum);
	              if(!(hMap.containsKey(sum)))
	            	  hMap.put(sum, newEntry);
	            
	          
	            
					/*
					 * System.out.println("before adding to parent -"+subList.size());
					 * 
					 * System.out.println("Added -- ");
					 */
	         
	        } else if (start <= end) {
	           
	            if(subList.size() >= subSetSize)
	            {
	                subList.remove(1);
	            }
	           
	            if(index == 0 )
	            {
	                subList = new ArrayList<Integer>();
	            }

	             subList.add(integers.get(start));
	           
	          //   System.out.println("after adding to sublist -"+subList);
	             
	           //   System.out.println("index 2 -"+index);
	           
	         //   for(Integer inte : subList) {
	           // System.out.println(inte);
	      //  }

	           
	            helper(hMap, combinations, subList, start + 1, end, index + 1, integers, subSetSize);
	            helper(hMap, combinations, subList, start + 1, end, index, integers,subSetSize);
	        }
	    }


}
