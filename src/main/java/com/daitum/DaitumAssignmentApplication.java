package com.daitum;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DaitumAssignmentApplication {

	public static void main(String[] args) {
		SpringApplication.run(DaitumAssignmentApplication.class, args);
	}

}
