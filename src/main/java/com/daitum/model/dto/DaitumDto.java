package com.daitum.model.dto;

import java.util.List;

public class DaitumDto {


	String subset;
	String sum;
	String input;
	String subsetsize;
	
	
	public String getSubset() {
		return subset;
	}
	public void setSubset(String subset) {
		this.subset = subset;
	}
	public String getSum() {
		return sum;
	}
	public void setSum(String sum) {
		this.sum = sum;
	}
	public String getInput() {
		return input;
	}
	public void setInput(String input) {
		this.input = input;
	}
	public String getSubsetsize() {
		return subsetsize;
	}
	public void setSubsetsize(String subsetsize) {
		this.subsetsize = subsetsize;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("DaitumDto [subset=");
		builder.append(subset);
		builder.append(", sum=");
		builder.append(sum);
		builder.append(", input=");
		builder.append(input);
		builder.append(", subsetsize=");
		builder.append(subsetsize);
		builder.append("]");
		return builder.toString();
	}
	
	
	
	

	
	
}
